Name:          ukui-media
Version:       3.1.1.1
Release:       4
Summary:       UKUI media utilities
License:       GPL-2.0-or-later and GPL-3.0-or-later and BSD-3-Clause and LGPL-2+
URL:           http://www.ukui.org
Source0:       https://gitee.com/openkylin/ukui-media/tags/%{name}-%{version}.tar.gz
Patch01:       0001-fix-ukui-media-lrelease-issue.patch
Patch02:       fix-ukui-media-3.1.1.1-build-error.patch
Patch03:       0001-fix-ukui-media-3.1.1.1-SEGV-coredump.patch
Patch04:       ukui-media-3.1.1.1-Fix-the-extra-slider.patch

Autoreq:       yes

BuildRequires: intltool
BuildRequires: qt5-qtbase-devel
BuildRequires: gsettings-qt-devel
BuildRequires: glib2-devel
BuildRequires: gsettings-qt-devel
BuildRequires: glib2-devel
BuildRequires: libxml2-devel
BuildRequires: qt5-qtsvg-devel
BuildRequires: libqtxdg-devel
BuildRequires: qt5-qtmultimedia-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: ukui-interface
BuildRequires: qt5-qttools-devel
BuildRequires: alsa-lib-devel
BuildRequires: pulseaudio-libs-devel
BuildRequires: dconf-devel
BuildRequires: libcanberra-devel
BuildRequires: libukcc-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: libkysdk-qtwidgets-devel >= 1.0.0
BuildRequires: libkysdk-waylandhelper-devel
BuildRequires: libsndfile-devel

Requires: ukui-media-common = %{version}
Requires: ukui-control-center >= 3.1.1
Requires: glib2
Requires: libcanberra

Recommends: alsa-utils sound-theme-freedesktop

%description
 A simple and lightweight screensaver written by Qt5.
 The screensaver supports biometric auhentication which is
 provided by biometric-auth service.

%package common
Summary:	UKUI media utilities (common files)
%description common
 UKUI media utilities are the audio mixer and the volume
 control applet.
 .
 This package contains the common files.

%prep
%autosetup -n %{name}-%{version} -p1

%build
mkdir build && pushd build
%{qmake_qt5} ..
%{make_build}
popd

%install
rm -rf $RPM_BUILD_ROOT
pushd build
%{make_install} INSTALL_ROOT=$RPM_BUILD_ROOT
popd

mkdir -p %{buildroot}/usr/share/man/man1/
gzip -c %{_builddir}/%{name}-%{version}/man/ukui-media-control-led.1      > %{buildroot}/usr/share/man/man1/ukui-media-control-led.1.gz
gzip -c %{_builddir}/%{name}-%{version}/man/ukui-volume-control-applet-qt.1      > %{buildroot}/usr/share/man/man1/ukui-volume-control-applet-qt.1.gz

mkdir -p %{buildroot}/usr/share/ukui-media/translations/audio
cp -r %{_builddir}/%{name}-%{version}/audio/translations/* %{buildroot}/usr/share/ukui-media/translations/audio
cp -r %{_builddir}/%{name}-%{version}/ukui-volume-control-applet-qt/translations/*.qm %{buildroot}/usr/share/ukui-media/translations/

mkdir -p %{buildroot}/usr/share/ukui-media/img
mkdir -p %{buildroot}/usr/share/ukui-media/qss
mkdir -p %{buildroot}/usr/share/ukui-media/sounds
mkdir -p %{buildroot}/usr/share/ukui/sounds
mkdir -p %{buildroot}/usr/share/sounds
mkdir -p %{buildroot}/usr/share/glib-2.0/schemas
mkdir -p %{buildroot}/usr/share/ukui-media/scripts
mkdir -p %{buildroot}/lib/systemd/system

mv  %{buildroot}/img %{buildroot}/usr/share/ukui-media/img
mv %{buildroot}/org.ukui.audio.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas
mv %{buildroot}/org.ukui.sound.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas
mv %{buildroot}/qss/* %{buildroot}/usr/share/ukui-media/qss
mv %{buildroot}/sounds/ukui-sound.xml %{buildroot}/usr/share/ukui-media/sounds
mv %{buildroot}/sounds/xunguang.xml %{buildroot}/usr/share/sounds

cp -r %{_builddir}/%{name}-%{version}/scripts/detection_output_mode.sh %{buildroot}/usr/share/ukui-media/scripts
cp -r %{_builddir}/%{name}-%{version}/data/org.ukui.media.sound.gschema.xml %{buildroot}/usr/share/glib-2.0/schemas
cp -r %{_builddir}/%{name}-%{version}/data/ukui-media-control-mute-led.service %{buildroot}/lib/systemd/system

mkdir -p %{buildroot}/etc/xdg/autostart
cp %{_builddir}/%{name}-%{version}/data/ukui-volume-control-applet.desktop %{buildroot}/etc/xdg/autostart

%clean
rm -rf $RPM_BUILD_ROOT

%post
set -e
glib-compile-schemas /usr/share/glib-2.0/schemas/ &> /dev/null ||:


%files
%{_bindir}/ukui-volume-control-applet-qt
%{_bindir}/ukui-media-control-led
%{_bindir}/ukui-login-sound
%{_datadir}/ukui-media/translations/
%{_datadir}/ukui-media/img/
%{_datadir}/ukui-media/qss/
%{_datadir}/ukui-media/sounds/*
%{_datadir}/sounds/*
%{_datadir}/ukui-media/scripts/
%{_datadir}/glib-2.0/schemas/org.ukui.audio.gschema.xml
%{_datadir}/glib-2.0/schemas/org.ukui.media.sound.gschema.xml
%{_datadir}/glib-2.0/schemas/org.ukui.sound.gschema.xml
/lib/systemd/system/ukui-media-control-mute-led.service
%{_libdir}/ukui-control-center/libaudio.so
%exclude /translations/*.qm

%files common
%{_sysconfdir}/xdg/autostart/
%{_datadir}/man/man1/ukui-media-control-led.1.gz
%{_datadir}/man/man1/ukui-volume-control-applet-qt.1.gz

%changelog
* Thu Feb 27 2025 peijiankang <peijiankang@kylinos.cn> - 3.1.1.1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add ukui-volume-control-applet-qt translations

* Tue Nov 19 2024 huayadong <huayadong@kylinos.cn> - 3.1.1.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: ukui-media-3.1.1.1-Fix-the-extra-slider.patch

* Wed Sep 04 2024 peijiankang <peijiankang@kylinos.cn> - 3.1.1.1-2
- Type:update
- ID:NA
- SUG:NA
- DESC: 0001-fix-ukui-media-3.1.1.1-SEGV-coredump.patch

* Thu Apr 04 2024 douyan <douyan@kylinos.cn> - 3.1.1.1-1
- Type:update
- ID:NA
- SUG:NA
- DESC: update to upstream version 3.1.1.1-ok7

* Fri Feb 10 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.0-4
- fix coredump of ukui-volume-control-applet-qt

* Tue Jan 3 2023 lvfei <lvfei@kylinos.cn> - 3.1.0-3
- update Source0 Url

* Tue Jan 3 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.0-2
- fix ukui-volume-control-applet-qt work error

* Mon Dec 5 2022 peijiankang <peijiankang@kylinos.cn> - 3.1.0-1
- update version to 3.1.0

* Thu Jul 28 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-18
- Add compilation dependencies: libpulse

* Thu Jul 28 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-17
- modify and update desktop file

* Thu Apr 28 2022 wangyueliang <wangyueliang@kylinos.cn> - 3.0.2-16
- Improve the project according to the requirements of compliance improvement.

* Tue Apr 19 2022 pei-jiankang <peijiankang@kylinos.cn> - 3.0.2-15
- modify ukui-media install error

* Sat Apr 02 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-14
- add yaml file

* Mon Mar 28 2022 huayadong <huayadong@kylinos.cn> - 3.0.2-13
- Add DBUS interface to send plug-in signal for Kirin recording 

* Mon Sep 27 2021 peijiankang<peijiankang@kylinos.cn> - 3.0.2-12
- repair the problem of adjusting animation freeze

* Sun Sep 26 2021 peijiankang<peijiankang@kylinos.cn> - 3.0.2-11
- return version to 3.0.2-2

* Sun Sep 26 2021 peijiankang<peijiankang@kylinos.cn> - 3.0.2-10
- repair the input device is not find of recorder

* Wed Jul 14 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-9 
- Set the app name and icon for Kirin Recording

* Tue Jul 13 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-8
- fix probabilistic crash when keyboard keys are mute

* Mon Jul 12 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-7
- Solve the problem of dragging the slider to adjust the microphone volume

* Mon Jul 12 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-6
- Solve the problem of adjusting animation freeze to adjust the sound

* Fri Jul 9 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-5
- update gsetting add org.ukui.sound.gschema.xml 

* Fri Jul 9 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-4
- Add a detection mechanism to terminate when PulseAudio exits abnormally

* Thu Jul 8 2021 tanyulong<tanyulong@kylinos.cn> - 3.0.2-3
- Fix the right menu without frosted glass

* Mon Dec 7 2020 lvhan <lvhan@kylinos.cn> - 3.0.2-2
- fix vol icon bug

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.2-1
- update to upstream version 3.0.1-1+1026

* Mon Sep 14 2020 douyan <douyan@kylinos.cn> - 2.0.4-1
- update to upstream version 2.0.4-2+0806

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 2.0.3-1
- Init package for openEuler
